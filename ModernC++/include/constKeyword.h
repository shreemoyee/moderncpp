
#ifndef constKeyword_h
#define constKeyword_h

class UseConst{
private:
    int age;
    std::string name;
    mutable int callCount=0;
public:
    UseConst(int x_):age(x_), name("null") {}
    UseConst(std::string name_): age(-1), name(name_) {}
    UseConst(int age_, std::string name_): age(age_), name(name_) {}
    void setAge(const int& x){
        std::cout<<"new age is "<<x<<std::endl;
        age = x;
        
    }
    const int getAge() const{
        callCount+=1; //mutable keyword member CAN be modified in a function with const
        return age;
    }
    int callCountCount() const{
        return callCount;
    }
};
#endif /* constKeyword_h */
