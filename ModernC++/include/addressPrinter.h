#ifndef pointer_h
#define pointer_h

#include <iostream>
void printAddress(int x){
    auto* ptr = &x;
    std::cout<<"Int passed "<<ptr<<std::endl;
}
void printAddress(double x){
    auto* ptr = &x;
    std::cout<<"Double passed "<<ptr<<std::endl;
}
void printAddress(std::string x){
    auto* ptr = &x;
    std::cout<<"String passed "<<ptr<<std::endl;
}
void printAddress(int* x){
    auto* ptr = &x;
    std::cout<<"Pointer passed! "<<ptr<<std::endl;
}


#endif /* pointer_h */
