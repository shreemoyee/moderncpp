#ifndef entity_h
#define entity_h

class Entity{
private:
    int entity_ = 0; //Default value initialised
public:
    Entity(){
        std::cout<< "Constructing entity! "<<std::endl;
    }
    Entity(const int& x){
        entity_ = x;
        std::cout<< "Constructing entity with an integer! "<<std::endl;
    }
    Entity(const Entity& entity){ //Creating a copy constructor, though default would have sufficed here 
        entity_ = entity.entity_;
    }
    ~Entity(){
        std::cout<<"Destructing entity! "<<std::endl;
    }
    void getEntity() const{ //Described as const => can not assign data members here like entity_ = smthing
        std::cout<<"The entity is " << entity_ <<std::endl;
    }
    void setEntity(const int& ent) { // const reference parameter, will not be modified here
        entity_ = ent;
    }

};

#endif /* entity_h */
