#ifndef log_h
#define log_h

#include <iostream>
class Logger{
public:
    void setLevel(int x){
        logLevel = x;
    }
    void all(const char* message){
        if(logLevel>=0)
            std::cout<<" [ALL] "<<message<<std::endl;
    }
    void warning(const char* message){
        if(logLevel>=1)
            std::cout<<" [WARNING] "<<message<<std::endl;
    }
    void error(const char* message){
        if(logLevel>=2)
            std::cout<<" [ERROR] "<<message<<std::endl;
    }
    
private:
    int logLevel;
    enum levels{
        allLevel, warningLevel, errorLevel
    };
};

#endif /* log_h */
