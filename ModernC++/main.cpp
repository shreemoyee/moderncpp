#include <iostream>
#include <memory>

#include "include/log.h"
#include "include/addressPrinter.h"
#include "include/entity.h"
#include "include/constKeyword.h"

int main(int argc, const char * argv[]) {
    Logger log;
    log.setLevel(0);
    log.all("Hello,This is the main function!");
    printAddress(30.8);
    int  a = 30;
    printAddress(a);
    int* add = &a;
    printAddress(add);

    //Empty scope,  stack allocation
    {
        Entity e1;
        e1.getEntity();
        Entity e2 = e1;
        e2.setEntity(10); //Default copy constructor wpuld have done the job as well!!
        e1.getEntity();

    } //When scope ends, destructor called automatically

    //Empty scope heap allocation
    {
        Entity* e1 = new Entity(2); //dynamic allocation using the keyword "new"
        e1->getEntity();
        delete e1; //explicitely call this to deallocate the memory, else destructor won't be called
    }
    //Smart pointers 1 - unique_ptr
    {
        std::unique_ptr<Entity> e1  = std::make_unique<Entity>(6);
        e1->getEntity(); //unique_ptr is smart, it does not need to be deleted, it deallocates as it goes out of scope
        //However, uniuq_ptr can not be copied (it's copy constructor is deleted) because it points to an address and after going out of scope it frees the memory - what shall happen to the copied entity then?
        // So std::unique_ptr<Entity> e2 = e1 does NOT work
        std::unique_ptr<Entity> e2(std::move(e1));
        // this will fail because e1 has been freed e1->getEntity();
        e2->getEntity();
    }

//    Smart pointer 2 - shared_pointer
    {
        auto e1 = new Entity;
        std::shared_ptr<Entity> shared_e1(e1);
//        std::shared_ptr<Entity> shared_e2(e1);

    }
    {
        std::shared_ptr<Entity> shared_e1 = std::make_shared<Entity>(10);
        {
            std::shared_ptr<Entity> e2 = shared_e1; //shared pointer keeps a track of reference counts
            e2->setEntity(2); //this will alter shared_e1 as well since both refer to the one address location of entity
            shared_e1->getEntity();
            e2->getEntity();

        }
        //Destructor is called after this scope ends - conly when BOTH go out of scope
        std::cout<<"Destructor NOT called"<<std::endl;
    }
    {
       std::unique_ptr<Entity> e1  = std::make_unique<Entity>(6);
       std::shared_ptr<Entity> shared_e3 = std::move(e1);
    }

//    //smart pointers 3 - weak pointer
    {
        std::shared_ptr<Entity> shared_e1 = std::make_shared<Entity>(14);
        {
            std::weak_ptr<Entity> weak = shared_e1; //weak pointer does not increase the reference count
            //It kinda cheaks if our pointer is still alive, that's all, so can NOT do weak->getEntity();
            //So how can I use it, if it is indeed alive? Dereferencing is not an option. BUT:
            std::shared_ptr<Entity> new_shared = weak.lock();
            shared_e1 = nullptr;  //weak does NOT dangle yet
            new_shared = nullptr; //weak now dangles

            if(weak.expired())
                std::cout<<"My weak pointer is dangling :( "<<std::endl;
            new_shared = weak.lock(); //allowed - new_shared is null
            std::cout<<new_shared<<std::endl;
        }
    }
    //const keyword
    {
        UseConst object(23, "shreemoyee");
        const int age = object.getAge();
        std::cout<<"age is "<<age<<std::endl;
        std::cout<<"getter called "<<object.callCountCount()<<" times."<<std::endl;
        
    }
    std::cin.get();
    
    return 0;
}
